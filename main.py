import os
import ctypes
import pandas as pd
import tkinter as tk
from tkinter.filedialog import *
from ttkthemes import *
from tkinter.ttk import *

DF = pd.DataFrame()
files = []
last_file_dir = os.path.expanduser('~')
last_save_dir = os.path.expanduser('~')
format_flag = 0


def set_high_definition_for_win(root):
    try:  # >= win 8.1
        ctypes.windll.shcore.SetProcessDpiAwareness(2)
    except:  # win 8.0 or less
        ctypes.windll.user32.SetProcessDPIAware()
        ctypes.windll.shcore.SetProcessDpiAwareness(1)
    ScaleFactor = ctypes.windll.shcore.GetScaleFactorForDevice(0)
    root.tk.call('tk', 'scaling', ScaleFactor / 80)


def set_central_position(root, width, height):
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    root.geometry(
        f'{width}x{height}+{(screen_width-width) // 2}+{(screen_height-height)//2}')


def read_excel(filename):
    global format_flag
    sheets_all = pd.read_excel(filename, header=0, sheet_name=None)
    if len(sheets_all) == 3:
        format_flag = 0
        df = sheets_all['发货明细']
    else:
        format_flag = 1
        df = sheets_all[list(sheets_all.keys())[0]]
    return df


def transform_0(df):
    selected_columns = ['型号', '名称', '商品数量', '货款金额', '快递费']
    return df[selected_columns].loc[~pd.isna(df.iloc[:, 0]), :]


def transform_1(df):
    if '合计' in df.iloc[:, 0].to_list():
        row_of_heji = df.index[df.iloc[:, 0] == '合计'][0]
        body_end = row_of_heji - 1
        foot_start = row_of_heji + 1
    elif '公司名称' in df.iloc[:, 0].to_list():
        foot_start = df.index[df.iloc[:, 0] == '公司名称'][0]
        body_end = foot_start - 2
    else:
        raise RuntimeError('错误的excel文件格式')
    body = df.iloc[:body_end, :]
    if '开票名称' in body.columns:
        body.rename(columns={'开票名称': '产品名称'}, inplace=True)
    if '名称' in body.columns:
        body.rename(columns={'名称': '产品名称'}, inplace=True)
    foot = df.iloc[foot_start:, :]
    for i in range(foot.shape[0]):
        body[foot.iloc[i, 0]] = foot.iloc[i, 1]
    return body.loc[:, ~body.columns.str.contains('^Unnamed')]


def transform_data(filename):
    global format_flag
    df = read_excel(filename)
    if format_flag == 1:
        df = transform_1(df)
    else:
        df = transform_0(df)
    return df


def load_files():
    global files, DF, show, last_file_dir
    files = askopenfilenames(title=u'选择文件', filetypes=[('Excel 文件', ('.xls', '.xlsx'))],
                             initialdir=last_file_dir)

    df_list = []
    if files:
        last_file_dir = os.path.dirname(files[0])
        for i in files:
            try:
                df_list.append(transform_data(i))
            except Exception as e:
                print(repr(e))
                show.set(os.path.split(i)[-1])
                return
    if df_list:
        DF = pd.concat(df_list)
        show.set(f'{len(df_list)}个文件已合并')


def save_df():
    global DF, last_save_dir, show
    merge_file_name = '合并Excel'
    directory = askdirectory(title='选择保存路径', initialdir=last_save_dir)
    if directory:
        last_save_dir = directory
        merge_file_name += pd.to_datetime(
            'now').strftime('%Y%m%d%H%M%S') + '.xlsx'
        DF.to_excel(os.path.join(directory, merge_file_name), index=False)
        show.set(f'保存完成: {merge_file_name}')
        os.system(f'start {directory}')


root = ThemedTk(theme='arc', toplevel=True, themebg=True)
# root.option_add('*Font', 'SIMKAI 10 bold')
show = tk.StringVar()
show.set('0个文件')
root.title('合并表格')
root.iconbitmap(r'favicon.ico')
set_high_definition_for_win(root)
set_central_position(root, 300, 100)

Label(root, textvariable=show).pack(side=tk.BOTTOM, pady=10)
Button(root, text='选择文件', command=load_files).pack(side=tk.LEFT, expand=True)
Button(root, text='保存导出', command=save_df).pack(side=tk.LEFT, expand=True)

root.mainloop()
