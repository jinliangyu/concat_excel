altgraph==0.17
et-xmlfile==1.1.0
future==0.18.2
importlib-metadata==4.4.0
numpy==1.20.3
openpyxl==3.0.7
pandas==1.2.4
pefile==2021.5.24
pyinstaller==4.3
pyinstaller-hooks-contrib==2021.1
python-dateutil==2.8.1
pytz==2021.1
pywin32-ctypes==0.2.0
six==1.16.0
typing-extensions==3.10.0.0
xlrd==2.0.1
zipp==3.4.1
-e git://github.com/RedFantom/ttkthemes#egg=ttkthemes